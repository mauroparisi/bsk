package tdd.training.bsk;

public class Game {

	Frame[] turni = new Frame[10];
	int fasi=0;
	private int firstbo;
	
	private int secondbo;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
	 int i=0;
	 while(i!=10) {
	 turni[i]= new Frame(0,0);
	 i++;
	 }
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) {
		// To be implemented
		turni[fasi]= frame;
		fasi++;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) {
		// To be implemented
		
		return turni[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) {
		// To be implemented
		firstbo = firstBonusThrow;
	
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow)  {
		// To be implemented
	 secondbo= secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		// To be implemented
		return firstbo;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		// To be implemented
		return secondbo;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	//Scusi prof ho committato e non avevo butterfly attivo 
	public int calculateScore()  {
		int score=0;
		
		for(int i=0;i<turni.length;i++) {
			if(turni[i].isStrike()) {
				if(i==9) {
					score+=turni[i].getScore()+getFirstBonusThrow()+ getSecondBonusThrow();
				}
				else {
					
				
					if(i==8) {
						turni[i].setBonus(turni[i+1].getFirstThrow());	
						score+=turni[i].getScore()+getFirstBonusThrow();
						
					}else {
							
						
						if(turni[i].isStrike() && turni[i+1].isStrike()) {
							turni[i].setBonus(turni[i+1].getFirstThrow() + turni[i+2].getFirstThrow());	
							score+=turni[i].getScore();
							}else {
								
								
								turni[i].setBonus(turni[i+1].getFirstThrow() + turni[i+1].getSecondThrow());	
								score+=turni[i].getScore();
							
									}
					}
						
				}
				}else{
					
				if(turni[i].isSpare()) {
				if(i==9) {
					score+=turni[9].getScore() + getFirstBonusThrow();
				}
				else {
				turni[i].setBonus(turni[i+1].getFirstThrow());
				
				score+=turni[i].getScore();
				}}
			
			
				else {
			score+= turni[i].getScore();
			}
		}
			}
		return score;	
}
	}
